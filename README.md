# eeTestMagicTrick



## Getting started

This repository contains the ingredients for a simple mg5_aMC + pythia8 UNLOPS setup for e+e- > jets. To get started, consider doing the following:


- Get the MG5 version supporting ickkw=5 (add npLO and npNLO flags, nothing more) by running "bzr branch lp:~maddevelopers/mg5amcnlo/simple_unlops"
- Inside MG5, replace Template/NLO/SubProcesses/fastjetfortran_madfks_full.cc with the version given in this directory. This enables the use of the fastjet ee kt cut (jetalgo -999 for now)
- Run MG5 with with eeJets.mg5 runcard
- Compile main-amcatnlo-lhef-prepare.cc (include hepmc3 and gzip, and place along main-amcatnlo-lhe-prepare.cmnd and VetoSecondEmissionDyn.h) within pythia and process the LHE file produced by MG5 (final state multiplicity for np = 0 is 0)
- Do the actual merging run using the provided main89unlopsee.cmnd and main89
- Do comparison runs with plain pythia using eejets.cmnd and eejetsNoTrick.cmnd in main89
- Run rivet on both, using e.g. the following Analyses: ALEPH_2004_S5765862 DELPHI_1990_I297698 DELPHI_1996_S3430090 JADE_OPAL_2000_S4300807 L3_2004_I652683 OPAL_2004_S6132243

- Extra: To improve merging prediction, consider playing with the following: CMW vs no CMW, TimeShower:alphasorder,  Merging:incompleteScalePrescrip, Merging:unorderedASscalePrescrip, Merging:TMS, jet cut in MG5, ...

